# Minimalistic trivia

Code for Task7

## Deployed at:
 
### [GitlabPages](https://mrjimbeam.gitlab.io/task7/)
### [Heroku](https://still-fortress-49006.herokuapp.com/)
 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
